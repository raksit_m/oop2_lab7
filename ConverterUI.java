import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.*;

/**
 * Simple Converter GUI for converting unit to unit.
 * @author Raksit Mantanacharu
 *
 */
public class ConverterUI extends JFrame {

	private JButton convertButton;
	private JButton clearButton;
	private JTextField inputField1;
	private JTextField inputField2;
	private JComboBox<Unit> comboBox1;
	private JComboBox<Unit> comboBox2;
	private UnitConverter unitconverter;

	/**
	 * Constructor for initializing UnitConverter and calling initComponents.
	 * @param uc as UnitConverter from main application
	 */
	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;
		initComponents();
	}

	/**
	 * initialize components in the window.
	 */
	private void initComponents() {
		this.setTitle("Simple Converter");

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout();
		contents.setLayout(layout);

		inputField1 = new JTextField(10);
		contents.add(inputField1);
		ActionListener listenerIF1 = new ConvertButtonListener();
		inputField1.addActionListener(listenerIF1);

		comboBox1 = new JComboBox<Unit> (unitconverter.getUnit());
		contents.add(comboBox1);
		ActionListener listenerCB1 = new ConvertButtonListener();
		//		comboBox1.addActionListener(listenerCB1);

		JLabel label1 = new JLabel("=");
		contents.add(label1);
		inputField2 = new JTextField(10);
		inputField2.setEditable(false);
		contents.add(inputField2);

		comboBox2 = new JComboBox<Unit> (unitconverter.getUnit());
		contents.add(comboBox2);
		ActionListener listenerCB2 = new ConvertButtonListener();
		//		comboBox2.addActionListener(listenerCB2);

		convertButton = new JButton("Convert");
		contents.add(convertButton);
		ActionListener listenerConvert = new ConvertButtonListener();
		convertButton.addActionListener(listenerConvert);

		clearButton = new JButton("Clear");
		contents.add(clearButton);
		ActionListener listenerClear = new ClearTextFieldListener();
		clearButton.addActionListener(listenerClear);

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	class ConvertButtonListener implements ActionListener {
		/** method to perform action when the button/comboBox/textField is pressed.
		 * @throws NumberFormatException
		 */
		
		public void actionPerformed(ActionEvent evt) {
			String s = inputField1.getText().trim();

			try {
				if(s.length() > 0) {
					double value = Double.valueOf(s);
					if(value >= 0) {
						Unit fromUnit = (Unit) comboBox1.getSelectedItem();
						Unit toUnit = (Unit) comboBox2.getSelectedItem();
						double result = unitconverter.convert(value, fromUnit, toUnit);
						inputField1.setForeground(Color.BLACK);
						inputField2.setText(String.format("%.6f", result));
					}
					else {
						throw new NumberFormatException();
					}
				}
			}	
			catch(NumberFormatException e) {
				inputField1.setForeground(Color.RED);
			}	
		}
	}

	class ClearTextFieldListener implements ActionListener {
		/** method to clear the textField. */
		public void actionPerformed(ActionEvent evt) {
			inputField1.setText("");
			inputField2.setText("");
		}
	}
}
