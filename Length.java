/**
 * Length class which can convert to unit to the another.
 * @author Raksit Mantanacharu
 *
 */
public enum Length implements Unit {
	METER("Meter",1.00),
	CENTIMETER("Centimeter",0.01),
	KILOMETER("Kilometer",1000.0),
	MILE("Mile",1609.344),
	FOOT("Foot",0.30480),
	WA("Wa",2.0);

	public final String name;
	public final double value;

	private Length(String name, double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * The process of converting a unit:
	 * - convert input from current unit to base unit
	 * - convert from base unit to desired unit.
	 * @param u as unit for converting
	 * @param amt (amount)
	 * @return the answer of converting
	 */
	public double convertTo(Unit u, double amt) {
		return this.getValue()*amt / u.getValue();
	}

	/**
	 * @return value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @return name
	 */
	public String toString() {
		return name;
	}

}
