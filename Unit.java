/**
 * An unit which can convert and show the result.
 * @author Raksit Mantanacharu
 *
 */
public interface Unit {
	/**
	 * Converting to an unit to the another one.
	 * @param u as an unit
	 * @param amt as amount
	 * @return an answer of converting
	 */
	double convertTo(Unit u, double amt);
	
	/**
	 * 
	 * @return value
	 */
	double getValue();
	
	/**
	 * 
	 * @return name
	 */
	String toString();
}
