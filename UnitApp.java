import java.util.Arrays;

/**
 * An application class for UnitConverter.
 * @author Raksit Mantanacharu
 *
 */
public class UnitApp {
	
	/**
	 * Main application.
	 * @param args for command line.
	 */
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		ConverterUI converter = new ConverterUI(uc);		
	}
}
