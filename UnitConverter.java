/**
 * Receiving the request from UI to convert a value.
 * @author Raksit Mantanacharu
 *
 */
public class UnitConverter {

	/**
	 * A convert method (it will call convertTo in Length).
	 * @param amount (how long it is)
	 * @param fromUnit (unit which user would like to input)
	 * @param toUnit (unit which user would like to get an answer)
	 * @return an answer as double
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit) {
		return fromUnit.convertTo(toUnit, amount);
	}
	
	/**
	 * 
	 * @return Array of Length enum
	 */
	public Unit[] getUnit() {
		return Length.values();
	}
}
